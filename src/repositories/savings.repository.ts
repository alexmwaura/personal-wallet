import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {PersonalWalletDataSource} from '../datasources';
import {Savings, SavingsRelations} from '../models';

export class SavingsRepository extends DefaultCrudRepository<
  Savings,
  typeof Savings.prototype.id,
  SavingsRelations
> {
  constructor(
    @inject('datasources.personalWallet') dataSource: PersonalWalletDataSource,
  ) {
    super(Savings, dataSource);
  }
}
