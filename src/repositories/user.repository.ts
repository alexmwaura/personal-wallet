import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {PersonalWalletDataSource} from '../datasources';
import {UserModel, UserRelations, Expense, Savings} from '../models';
import {ExpenseRepository} from './expense.repository';
import {SavingsRepository} from './savings.repository';

export class UserRepository extends DefaultCrudRepository<
  UserModel,
  typeof UserModel.prototype.id,
  UserRelations
> {

  public readonly expenses: HasManyRepositoryFactory<Expense, typeof UserModel.prototype.id>;

  public readonly savings: HasManyRepositoryFactory<Savings, typeof UserModel.prototype.id>;

  constructor(
    @inject('datasources.personalWallet') dataSource: PersonalWalletDataSource, @repository.getter('ExpenseRepository') protected expenseRepositoryGetter: Getter<ExpenseRepository>, @repository.getter('SavingsRepository') protected savingsRepositoryGetter: Getter<SavingsRepository>,
  ) {
    super(UserModel, dataSource);
    this.savings = this.createHasManyRepositoryFactoryFor('savings', savingsRepositoryGetter,);
    this.registerInclusionResolver('savings', this.savings.inclusionResolver);
    this.expenses = this.createHasManyRepositoryFactoryFor('expenses', expenseRepositoryGetter,);
    this.registerInclusionResolver('expenses', this.expenses.inclusionResolver);
  }
}
