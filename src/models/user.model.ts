import {User} from '@loopback/authentication-jwt';
import { model, property, hasMany} from '@loopback/repository';
import {Expense} from './expense.model';
import {Savings} from './savings.model';

@model({
  settings: {
    indexes: {
      uniqueEmail: {
        keys: {
          email: 1,
        },
        options: {
          unique: true,
        },
      },
    },
    hiddenProperties: ['password'],
  },
})
export class UserModel extends User {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id: string;

  username: string;
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @hasMany(() => Expense)
  expenses: Expense[];

  @hasMany(() => Savings)
  savings: Savings[];

  constructor(data?: Partial<UserModel>) {
    super(data);
  }
}



export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
export type Credentials = {
  username: string;
  password: string;
};
