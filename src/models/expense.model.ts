import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Expense extends Entity {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      type: 'string',
      enum: ['saundry', 'foodAndClothing', 'housing', 'others']
    }
  })
  description: string;

  @property({
    type: 'string',
    required: true,
  })
  amount: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  userModelId?: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Expense>) {
    super(data);
  }
}

export interface ExpenseRelations {
  // describe navigational properties here
}

export type ExpenseWithRelations = Expense & ExpenseRelations;
