import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';
import {mongoPassword} from '../passwords';

const config = {
  name: 'personalWallet',
  connector: 'mongodb',
  url: `mongodb+srv://alex:${mongoPassword}@cluster0.hblbc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
  host: '',
  port: 27017,
  user: '',
  password: '',
  database: 'personalWallet',
  useNewUrlParser: true
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class PersonalWalletDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'personalWallet';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.personalWallet', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
