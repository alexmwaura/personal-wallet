import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  UserModel,
  Savings,
} from '../models';
import {UserRepository} from '../repositories';

export class UserSavingsController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) { }

  @get('/users/{id}/savings', {
    responses: {
      '200': {
        description: 'Array of User has many Savings',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Savings)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Savings>,
  ): Promise<Savings[]> {
    return this.userRepository.savings(id).find(filter);
  }

  @post('/users/{id}/savings', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(Savings)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof UserModel.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Savings, {
            title: 'NewSavingsInUser',
            exclude: ['id'],
            optional: ['userId']
          }),
        },
      },
    }) savings: Omit<Savings, 'id'>,
  ): Promise<Savings> {
    return this.userRepository.savings(id).create(savings);
  }

  @patch('/users/{id}/savings', {
    responses: {
      '200': {
        description: 'User.Savings PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Savings, {partial: true}),
        },
      },
    })
    savings: Partial<Savings>,
    @param.query.object('where', getWhereSchemaFor(Savings)) where?: Where<Savings>,
  ): Promise<Count> {
    return this.userRepository.savings(id).patch(savings, where);
  }

  @del('/users/{id}/savings', {
    responses: {
      '200': {
        description: 'User.Savings DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Savings)) where?: Where<Savings>,
  ): Promise<Count> {
    return this.userRepository.savings(id).delete(where);
  }
}
