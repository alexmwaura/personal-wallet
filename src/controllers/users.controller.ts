import {
  authenticate,
  TokenService,
  UserService,
} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {del, get, getModelSchemaRef, post, requestBody} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {
  PasswordHasherBindings,
  TokenServiceBindings,
  UserServiceBindings,
} from '../keys';
import {Credentials, UserModel} from '../models';
import {UserRepository} from '../repositories';
import {PasswordHasher} from '../services';
import {LOGGED_IN} from '../spec/';
import {HttpErrors} from '@loopback/rest';


type AuthResponse = {
  token: string;
  expires: Date;
  user: UserModel;
};

export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public tokenService: TokenService,
    @inject(TokenServiceBindings.TOKEN_EXPIRES_IN)
    public expiresIn: number,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<UserModel, Credentials>,
  ) {}

  @post('/signup', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(UserModel, {exclude: ['password']}),
          },
        },
      },
    },
  })
  async signup(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            required: ['email', 'password', 'username'],
            properties: {
              email: {type: 'string', format: 'email'},
              password: {
                type: 'string',
                pattern:
                  '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{5,512})',
                minLength: 8,
                maxLength: 512,
              },
              username: {type: 'string'},
            },
          },
        },
      },
    })
    user: UserModel,
  ): Promise<UserModel> {
    const uniqueEmail =  await this.userRepository.findOne({
      where: {email: user.email}
    })
    if(uniqueEmail){
      throw new HttpErrors.Unauthorized("email already exiss");

    }
    const password = await this.passwordHasher.hashPassword(user.password);
    const profile = this.userService.convertToUserProfile(user);
    const token = await this.tokenService.generateToken(profile);
    const expires = new Date();
    expires.setTime(expires.getTime() + this.expiresIn * 1000);
    const newUser = this.userRepository.create({
      ...user,
      password,
    });
    return {
      ...newUser,
      token,
    };
  }

  @post('/login', {
    responses: {
      '200': {
        description: 'User model instance and JWT token',
        content: {
          'application/json': {
            schema: {
              token: 'string',
              expires: 'date',
              user: getModelSchemaRef(UserModel, {exclude: ['password']}),
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['username', 'password'],
            properties: {
              username: {
                type: 'string',
              },
              password: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    creds: Credentials,
  ): Promise<AuthResponse> {
    const user = await this.userService.verifyCredentials(creds);

    const profile = this.userService.convertToUserProfile(user);

    const token = await this.tokenService.generateToken(profile);

    const expires = new Date();
    expires.setTime(expires.getTime() + this.expiresIn * 1000);

    return {
      token,
      expires,
      user,
    };
  }

  @get('/renewToken', {
    security: LOGGED_IN,
    responses: {
      '200': {
        description: 'Renewed JWT token',
        content: {
          'application/json': {
            schema: {
              token: 'string',
              expires: 'date',
              user: getModelSchemaRef(UserModel, {exclude: ['password']}),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async renewToken(
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<AuthResponse> {
    const user = await this.userRepository.findById(profile[securityId]);

    const newProfile = this.userService.convertToUserProfile(user);

    const token = await this.tokenService.generateToken(newProfile);

    const expires = new Date();
    expires.setTime(expires.getTime() + this.expiresIn * 1000);

    return {
      token,
      expires,
      user,
    };
  }

  @del('/users/', {
    title: 'Delete the logged in user',
    security: LOGGED_IN,
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteMe(
    @inject(SecurityBindings.USER) profile: UserProfile,
  ): Promise<void> {
    await this.userRepository.deleteById(profile.id);
  }
}
