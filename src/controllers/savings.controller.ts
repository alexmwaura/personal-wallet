import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Savings} from '../models';
import {SavingsRepository} from '../repositories';

export class SavingsController {
  constructor(
    @repository(SavingsRepository)
    public savingsRepository : SavingsRepository,
  ) {}

  @post('/savings')
  @response(200, {
    description: 'Savings model instance',
    content: {'application/json': {schema: getModelSchemaRef(Savings)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Savings, {
            title: 'NewSavings',
            exclude: ['id'],
          }),
        },
      },
    })
    savings: Omit<Savings, 'id'>,
  ): Promise<Savings> {
    return this.savingsRepository.create(savings);
  }

  @get('/savings/count')
  @response(200, {
    description: 'Savings model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Savings) where?: Where<Savings>,
  ): Promise<Count> {
    return this.savingsRepository.count(where);
  }

  @get('/savings')
  @response(200, {
    description: 'Array of Savings model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Savings, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Savings) filter?: Filter<Savings>,
  ): Promise<Savings[]> {
    return this.savingsRepository.find(filter);
  }

  @patch('/savings')
  @response(200, {
    description: 'Savings PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Savings, {partial: true}),
        },
      },
    })
    savings: Savings,
    @param.where(Savings) where?: Where<Savings>,
  ): Promise<Count> {
    return this.savingsRepository.updateAll(savings, where);
  }

  @get('/savings/{id}')
  @response(200, {
    description: 'Savings model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Savings, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Savings, {exclude: 'where'}) filter?: FilterExcludingWhere<Savings>
  ): Promise<Savings> {
    return this.savingsRepository.findById(id, filter);
  }

  @patch('/savings/{id}')
  @response(204, {
    description: 'Savings PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Savings, {partial: true}),
        },
      },
    })
    savings: Savings,
  ): Promise<void> {
    await this.savingsRepository.updateById(id, savings);
  }

  @put('/savings/{id}')
  @response(204, {
    description: 'Savings PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() savings: Savings,
  ): Promise<void> {
    await this.savingsRepository.replaceById(id, savings);
  }

  @del('/savings/{id}')
  @response(204, {
    description: 'Savings DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.savingsRepository.deleteById(id);
  }
}
