import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  UserModel,
  Expense,
} from '../models';
import {UserRepository} from '../repositories';

export class UserExpenseController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) { }

  @get('/users/{id}/expenses', {
    responses: {
      '200': {
        description: 'Array of UserModel has many Expense',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Expense)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Expense>,
  ): Promise<Expense[]> {
    return this.userRepository.expenses(id).find(filter);
  }

  @post('/users/{id}/expenses', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(Expense)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof UserModel.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Expense, {
            title: 'NewExpenseInUser',
            exclude: ['id'],
            optional: ['userId']
          }),
        },
      },
    }) expense: Omit<Expense, 'id'>,
  ): Promise<Expense> {
    return this.userRepository.expenses(id).create(expense);
  }

  @patch('/users/{id}/expenses', {
    responses: {
      '200': {
        description: 'User.Expense PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Expense, {partial: true}),
        },
      },
    })
    expense: Partial<Expense>,
    @param.query.object('where', getWhereSchemaFor(Expense)) where?: Where<Expense>,
  ): Promise<Count> {
    return this.userRepository.expenses(id).patch(expense, where);
  }

  @del('/users/{id}/expenses', {
    responses: {
      '200': {
        description: 'User.Expense DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Expense)) where?: Where<Expense>,
  ): Promise<Count> {
    return this.userRepository.expenses(id).delete(where);
  }
}
